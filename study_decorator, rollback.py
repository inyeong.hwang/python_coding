import psycopg2 as pg2


def connect():
    con = pg2.connect(host="127.0.0.1",
                      port="5432",
                      database="pglstudy",
                      user="postgres",
                      password="postgres")
    return con


def exceptionn(func):
    def wrapper(*args, **kwargs):
        try:
            return func(*args)
        except:
            con = args[0] # con = kwargs['connect']
            # con.rollback()
            return list()
    return wrapper


@exceptionn
def select(con):
    sql_select = f"SELECT " \
                 f"namee, " \
                 f"FROM test"
    cur = con.cursor()
    cur.execute(sql_select)
    rs = cur.fetchall()
    cur.close()
    return rs


@exceptionn
def update(con):
    sql_update = f"UPDATE test SET"
    sql_update += f" name = 'iii'" \
                  f" WHERE" \
                  f" name='yi'"
    cur = con.cursor()
    cur.execute(sql_update)
    cur.close()
    con.commit()


def run(): # ex.dzbmanager / process_request_batch()
    con = connect()
    select(con)  #connect=con (kwargs)
    # con = connect()
    update(con)

run()

