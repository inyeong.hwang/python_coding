from multiprocessing import Process, connection, current_process
from time import sleep
from datetime import datetime
import time


def one(a):
    print(a+"task_one")
    time.sleep(10)
    print(a+"task_one end")


def two(a):
    print(a+"task two")
    print(a+"task two end") 

def three(a):
    print(a+"task_three")
    print(a+"task two end") 

def run_task_grade():
    print(f"{datetime.now()} {current_process().name} grade")
    p_1= Process(target=one, args=("grade ",))
    p_2= Process(target=two, args=("grade ",))
    p_3= Process(target=three, args=("grade ",))
    p_1.start()
    p_2.start()
    p_3.start()

    time.sleep(15)
    print(p_1.is_alive())
    print(p_2.is_alive())
    print(p_3.is_alive())

def run_task_category():
    print(f"{datetime.now()} {current_process().name} category")
    p_1= Process(target=one, args=("category ",))
    p_2= Process(target=two, args=("category ",))
    p_3= Process(target=three, args=("category ",))
    p_1.start()
    p_2.start()
    p_3.start()

    time.sleep(15)
    print(p_1.is_alive())
    print(p_2.is_alive())
    print(p_3.is_alive())

def run_task_analysis():
    print(f"{datetime.now()} {current_process().name} analysis")
    p_1= Process(target=one, args=("analysis ",))
    p_2= Process(target=two, args=("analysis ",))
    p_3= Process(target=three, args=("analysis ",))
    p_1.start()
    p_2.start()
    p_3.start()

    time.sleep(15)
    print(p_1.is_alive())
    print(p_2.is_alive())
    print(p_3.is_alive())

def get_task_process(task_group):
        if task_group == "grade":
            p = Process(target=run_task_grade)
            return p
        if task_group == "category": 
            p = Process(target=run_task_category) # , daemon=True
            return p
        if task_group == "analysis":
            p = Process(target=run_task_analysis)
            return p
        return None


if __name__ == '__main__':
    task_group_list = ["grade", "category", "analysis", "aaa"]

    task_pool = list()
    for task_group in task_group_list:
        task_process = get_task_process(task_group)

        if not task_process is None:
            task_pool.append(task_process)
    
    for p in task_pool:
        p.start()

    print(f"{datetime.now()} {current_process().name} 자식프로세스 start") #자식 프로세스 실행과 병렬 처리

    connection.wait(process.sentinel for process in task_pool)
    # time.sleep(20) #10초씩 병렬 실행 -> 15초후 자식프로세스 종료 확인 -> "20초 후 부모프로세스 종료 확인"
    # for p in task_pool:
    #     print(p.is_alive())

    print(f"{datetime.now()} {current_process().name} 자식프로세스 start 이후 코드") #connection.wait() 사용 시 자식 프로세스 종료까지 대기 후 실행